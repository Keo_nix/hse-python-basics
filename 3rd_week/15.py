s = input()
f_start = s.find('f')
f_end = len(s) - s[::-1].find('f') - 1
if f_start != -1:
    print(f_start, end=' ')
    if f_start != f_end:
        print(f_end)
