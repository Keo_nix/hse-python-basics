from math import ceil

x = float(input())
f = x - int(x)
if f < .5:
    print(int(x))
else:
    print(ceil(x))
