N = int(input())
N %= 24 * 60 * 60

s = N % 60
s1 = s // 10
s2 = s % 10

m = N // 60 % 60
m1 = m // 10
m2 = m % 10

h = N // (60 * 60)

print(h, ':', m1, m2, ':', s1, s2, sep='')
