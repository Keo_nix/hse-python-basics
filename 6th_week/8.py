input_file = open("input.txt", "r", encoding="utf8")
output_file = open("output.txt", "w", encoding="utf8")
a = []
for line in input_file:
    tmp = line.split()
    a.append((tmp[0], tmp[1], tmp[3]))
input_file.close()
a.sort()
for x in a:
    print(*x, file=output_file)
output_file.close()
