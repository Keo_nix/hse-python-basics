def merge(A, B):
    C = []
    i = j = 0
    while i < len(A) and j < len(B):
        if A[i] < B[j]:
            C.append(A[i])
            i += 1
        else:
            C.append(B[j])
            j += 1
    if i == len(A):
        for k in range(j, len(B)):
            C.append(B[k])
    else:
        for k in range(i, len(A)):
            C.append(A[k])
    return C


A = list(map(int, input().split()))
B = list(map(int, input().split()))

print(*merge(A, B))
