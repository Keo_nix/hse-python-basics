def count_sort(a):
    count_list = [0] * 101
    for now in a:
        count_list[now] += 1
    a.clear()
    for i in range(len(count_list)):
        for j in range(count_list[i]):
            a.append(i)


input_list = list(map(int, input().split()))
count_sort(input_list)
print(*input_list)
