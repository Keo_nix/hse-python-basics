from operator import itemgetter

N = int(input())
my_list = []
for i in range(N):
    tmp = input().split()
    my_list.append((tmp[0], int(tmp[1])))
my_list.sort(key=itemgetter(1), reverse=True)
for now in my_list:
    print(now[0])
