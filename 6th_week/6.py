from operator import itemgetter

n = int(input())
a = list(map(int, input().split()))
for i in range(n):
    a[i] = a[i], i

m = int(input())
b = list(map(int, input().split()))
for i in range(m):
    b[i] = b[i], i

a.sort()
b.sort()

j = 0
for i in range(n):
    while j < m - 1 and abs(a[i][0] - b[j][0]) > abs(a[i][0] - b[j + 1][0]):
        j += 1
    if j < m - 1:
        a[i] = a[i] + (b[j][1] + 1,)
    else:
        for k in range(i, n):
            a[k] = a[k] + (b[j][1] + 1,)
        break

a.sort(key=itemgetter(1))

for x in a:
    print(x[2], end=' ')
