input_file = open("input.txt", "r", encoding="utf8")
K = int(input_file.readline())
a = []
for line in input_file:
    scores = tuple(map(int, line.split()[-3:]))
    if min(scores) >= 40:
        a.append(sum(scores))
input_file.close()
output_file = open("output.txt", "w", encoding="utf8")
if K >= len(a):
    print(0, file=output_file)
elif a.count(max(a)) > K:
    print(1, file=output_file)
else:
    a.sort(reverse=True)
    i = K - 1
    while a[i] == a[K]:
        i -= 1
    print(a[i], file=output_file)
output_file.close()
