N = int(input())
d1, d2 = dict(), dict()
for _ in range(N):
    w1, w2 = input().split()
    d1[w1] = w2
    d2[w2] = w1
word = input()
# print(d1.get(word, d2[word])) ???
print(d1.get(word, ''), d2.get(word, ''), sep='')
