from operator import itemgetter

fin = open('input.txt')
s = fin.read().split()
d = dict()
for word in s:
    d[word] = d.get(word, 0) + 1
lst = list(d.items())
lst.sort(key=itemgetter(0))
lst.sort(key=itemgetter(1), reverse=True)
for word in lst:
    print(word[0])
