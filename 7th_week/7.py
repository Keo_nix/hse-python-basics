n = int(input())
p_set = set(range(1, n + 1))
while True:
    s = input()
    if s == 'HELP':
        break
    if input() == 'YES':
        p_set &= set(map(int, s.split()))
    else:
        p_set -= set(map(int, s.split()))
print(*sorted(p_set))
