fin = open('input.txt')
s = fin.read().split()
d = dict()
for word in s:
    d[word] = d.get(word, 0) + 1
m = max(d.values())
lst = []
for key in d:
    if d[key] == m:
        lst.append(key)
print(min(lst))
