N = int(input())
s = [set() for _ in range(N)]
for i in range(N):
    for _ in range(int(input())):
        s[i].add(input())

total = s[0]
for i in range(1, N):
    total = total & s[i]
print(len(total))
print(*total, sep='\n')

total = s[0]
for i in range(1, N):
    total |= s[i]
print(len(total))
print(*total, sep='\n')
