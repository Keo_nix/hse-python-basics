from operator import itemgetter

fin = open('input.txt', encoding='utf8')
d = dict()
total = 0
for line in fin:
    d[line] = d.get(line, 0) + 1
    total += 1
fin.close()
lst = list(d.items())
lst.sort(key=itemgetter(1), reverse=True)
fout = open('output.txt', mode='w', encoding='utf8')
fout.write(lst[0][0])
if lst[0][1] <= total / 2:
    fout.write(lst[1][0])
fout.close()
