n_even = 0
while True:
    x = int(input())
    if not x:
        break
    if not x % 2:
        n_even += 1
print(n_even)
