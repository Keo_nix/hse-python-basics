x = int(input())

if x % 4:
    print('NO')
else:
    if x % 100:
        print('YES')
    else:
        if x % 400:
            print('NO')
        else:
            print('YES')
