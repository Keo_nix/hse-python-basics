n = int(input())
i = 2
n_sum = 1
while i <= n:
    n_sum += i ** 2
    i += 1
print(n_sum)
