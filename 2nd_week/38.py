max1 = max2 = -1
while True:
    x = int(input())
    if not x:
        break
    if x >= max1:
        max2 = max1
        max1 = x
    if max2 < x < max1:
        max2 = x
if max2 != -1:
    print(max2)
