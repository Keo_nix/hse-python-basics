A, B, C, D = int(input()), int(input()), int(input()), int(input())
E = int(input())

if A > B:
    (A, B) = (B, A)
if B > C:
    (B, C) = (C, B)
if A > B:
    (A, B) = (B, A)

if D >= A and E >= B or D >= B and E >= A:
    print('YES')
else:
    print('NO')
