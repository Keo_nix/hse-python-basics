N = int(input())
i = 1
while i ** 2 <= N:
    print(str(i ** 2) + ' ', end='')
    i += 1
