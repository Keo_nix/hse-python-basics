maximum = -1
n_max = 0
while True:
    x = int(input())
    if not x:
        break
    if x > maximum:
        maximum = x
        n_max = 1
    elif x == maximum:
        n_max += 1
print(n_max)
