n = n_tmp = 1
x_prev = 0
while True:
    x = int(input())
    if not x:
        break
    if x == x_prev:
        n_tmp += 1
        if n_tmp > n:
            n = n_tmp
    else:
        n_tmp = 1
    x_prev = x
print(n)
