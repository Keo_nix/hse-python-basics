def reduce_fraction(n, m):
    i = 2
    while i <= min(n, m):
        if n % i == 0 and m % i == 0:
            return reduce_fraction(n // i, m // i)
        i += 1
    return n, m


x = int(input())
y = int(input())

print(*reduce_fraction(x, y))
