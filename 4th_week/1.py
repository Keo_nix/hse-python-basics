def min4(a, b, c, d):
    m1 = min(a, b)
    m2 = min(c, d)
    return min(m1, m2)


a = int(input())
b = int(input())
c = int(input())
d = int(input())

print(min4(a, b, c, d))
