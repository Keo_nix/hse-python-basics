def min_diviser(n):
    i = 2
    while i <= n ** .5:
        if not (n % i):
            return i
        i += 1
    return n


x = int(input())

print(min_diviser(x))
