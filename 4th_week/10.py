def is_prime(n):
    i = 2
    while i <= n ** .5:
        if not (n % i):
            return False
        i += 1
    return True


x = int(input())

if is_prime(x):
    print('YES')
else:
    print('NO')
