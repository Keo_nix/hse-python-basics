def power(a, n):
    if n:
        return a * power(a, n - 1)
    else:
        return 1


x = float(input())
y = int(input())

print(power(x, y))
