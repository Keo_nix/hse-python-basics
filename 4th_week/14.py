def power(a, n):
    if n % 2:
        return a * power(a, n - 1)
    else:
        if n:
            return power(a ** 2, n / 2)
        else:
            return 1


x = float(input())
y = int(input())

print(power(x, y))
