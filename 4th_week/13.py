def sum1(a, b):
    if b:
        return sum1(a, b - 1) + 1
    else:
        return a


x = int(input())
y = int(input())

print(sum1(x, y))
