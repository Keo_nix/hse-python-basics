from functools import reduce

a = list(map(int, input().split()))

minimum = reduce(min, a)
i_min = a.index(minimum)

maximum = reduce(max, a)
i_max = a.index(maximum)

a[i_min], a[i_max] = a[i_max], a[i_min]

print(*a)
