A = list(map(int, input().split()))

x_prev = A[0]
for x in A[1:]:
    if x > x_prev:
        print(x, end=' ')
    x_prev = x
