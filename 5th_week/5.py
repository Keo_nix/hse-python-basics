n = int(input())

for i in range(0, 4):
    for j in range(1, n + 1):
        flag = ('+___', '|' + str(j) + ' /', '|__\\', '|   ')
        print(flag[i], end=' ')
    print()
