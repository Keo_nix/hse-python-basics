A = int(input())
B = int(input())

if A < B:
    my_range = range(A, B + 1)
else:
    my_range = range(A, B - 1, -1)

for i in my_range:
    print(i, end=' ')
