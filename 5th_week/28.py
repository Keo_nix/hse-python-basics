input()
a = list(map(int, input().split()))
x = int(input())
closest = a[0]
for i in a[1:]:
    if abs(x - i) < abs(x - closest):
        closest = i
print(closest)
