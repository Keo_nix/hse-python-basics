A = list(map(int, input().split()))

maximum = A[0]
n_max = 0
i = 1
for x in A[1:]:
    if x >= maximum:
        maximum = x
        n_max = i
    i += 1
print(maximum, n_max)
