A = list(map(int, input().split()))

x_pos_min = 1001
for x in A:
    if 0 < x < x_pos_min:
        x_pos_min = x

print(x_pos_min)
