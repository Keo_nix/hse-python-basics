from sys import stdin


class Matrix:
    def __init__(self, my_list):
        self.data = my_list[:]

    def __str__(self):
        s = []
        for row in self.data:
            s.append("\t".join(map(str, row)))
        return "\n".join(s)

    def __add__(self, other):
        return Matrix(list(
            map(lambda row: map(lambda x, y: x + y, self.data[row],
                                other.data[row]), range(len(self.data)))))

    def __mul__(self, scalar):
        return Matrix(list(
            map(lambda row: map(lambda x: x * scalar, self.data[row]),
                range(len(self.data)))))

    __rmul__ = __mul__

    def size(self):
        return len(self.data), len(self.data[0])


exec(stdin.read())
