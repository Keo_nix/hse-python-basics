from sys import stdin


class MatrixError(BaseException):
    def __init__(self, matrix1, matrix2):
        self.matrix1 = matrix1
        self.matrix2 = matrix2


class Matrix:
    def __init__(self, my_list):
        self.data = my_list[:]

    def __str__(self):
        s = []
        for row in self.data:
            s.append("\t".join(map(str, row)))
        return "\n".join(s)

    def __add__(self, other):
        if self.size() == other.size():
            return Matrix(list(
                map(lambda row: map(lambda x, y: x + y, self.data[row],
                                    other.data[row]), range(len(self.data)))))
        raise MatrixError(self, other)

    def __mul__(self, scalar):
        return Matrix(list(
            map(lambda row: map(lambda x: x * scalar, self.data[row]),
                range(len(self.data)))))

    __rmul__ = __mul__

    def size(self):
        return len(self.data), len(self.data[0])

    def transpose(self):
        self.data = list(map(list, zip(*self.data)))
        return self

    @staticmethod
    def transposed(matrix):
        return Matrix(list(map(list, zip(*matrix.data))))


exec(stdin.read())
