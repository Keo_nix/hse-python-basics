import copy
from sys import stdin


class Matrix:
    def __init__(self, my_list):
        self.data = copy.deepcopy(my_list)

    def __str__(self):
        s = []
        for row in self.data:
            s.append("\t".join(map(str, row)))
        return "\n".join(s)

    def size(self):
        return len(self.data), len(self.data[0])


exec(stdin.read())
